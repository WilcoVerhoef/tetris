module Tetris where
import Data.Maybe

data Tetromino = I | J | L | O | S | T | Z
data Rotation = R0 | R90 | R180 | R270

minos :: Tetromino -> [(Int,Int)]
minos I = [(0,0),(0,1),(0,2),(0,3)]
minos J = [(0,0),(1,0),(2,0),(2,1)]
minos L = [(0,1),(1,1),(2,1),(2,0)]
minos O = [(0,0),(0,1),(1,0),(1,1)]
minos S = [(0,0),(0,1),(1,1),(1,2)]
minos T = [(0,1),(1,0),(1,1),(1,2)]
minos Z = [(0,0),(0,1),(1,0),(1,1)]

instance Enum Rotation where
    succ R0 = R90 ; succ R90 = R180 ; succ R180 = R270 ; succ R270 = R0
    pred R0 = R270 ; pred R90 = R0 ; pred R180 = R90 ; pred R270 = R180
    fromEnum R0 = 0 ; fromEnum R90 = 1 ; fromEnum R180 = 2 ; fromEnum R270 = 3
    toEnum 0 = R0 ; toEnum 1 = R90 ; toEnum 2 = R180 ; toEnum 3 = R270
    toEnum _ = error "Rotation.toEnum"

data Faller = Faller {
    tetromino :: Tetromino,
    position :: (Int, Int),
    rotation :: Rotation
}

type Playfield = [[Maybe Tetromino]]

data Game = Game {
    playfield :: Playfield,
    faller :: Faller
}

left, right :: Faller -> Faller
left f @ Faller { position = (x, y) } = f { position = (x - 1, y) }
right f @ Faller { position = (x, y) } = f { position = (x + 1, y) }

rotateLeft, rotateRight :: Faller -> Faller
rotateLeft f @ Faller { rotation = r } = f { rotation = succ r }
rotateRight f @ Faller { rotation = r } = f { rotation = pred r }

fall :: Faller -> Faller
fall f @ Faller { position = (x, y) } = f { position = (x, y + 1) }

cells :: Faller -> [(Int, Int)]
cells f = map (\(x, y) -> (x + fx, y + fy)) (minos (tetromino f))
    where (fx, fy) = position f

collides :: Playfield -> Faller -> Bool
collides p f = any (\(x, y) -> isJust (p !! y !! x)) (cells f)